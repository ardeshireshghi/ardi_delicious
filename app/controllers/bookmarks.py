# Import flask dependencies
from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for
from app.lib.response import create_response
import json
import sys

# Import password / encryption helper tools
#from werkzeug import check_password_hash, generate_password_hash

# Import the database object from the main app module
#from app import db

# Import module forms
from app.mod_delicious.client import DeliciousApiClient

# Import module models (i.e. User)
#from app.mod_auth.models import User


# Define the blueprint: 'auth', set its url prefix: app.url/auth
bookmarks_bp = Blueprint('bookmarks', __name__, url_prefix = '/api')

# Set the route and accepted methods
@bookmarks_bp.route('/bookmarks/', methods=['GET', 'OPTIONS'])
def get_all():

    # If sign in form is submitted
    client = DeliciousApiClient(request)
    try:
        bookmarks = client.get_all_bookmarks().json_parse()
        return create_response(200, bookmarks, {'Content-type': 'application/json', 'Access-Control-Allow-Origin': '*'})
    except Exception as e:
        print e
        client_err = client.get_error()
        return create_response(client_err['status'],client_err['message'])
    

