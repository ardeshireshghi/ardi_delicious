from flask import make_response

def create_response(status_code = 200, data = '', headers = {}):

    resp = make_response(data, status_code)
    for header_name in headers:
        resp.headers[header_name] = headers[header_name]
    return resp

