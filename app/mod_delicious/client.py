# -*- coding: utf-8 -*-
import sys
from  app.config import API_USER, API_PASS, DELICIOUS_API_BASE_URL
import requests
from  base64 import b64encode
import xml.etree.ElementTree as ET
import json

class DeliciousRequest:
    def __init__(self, url, method = 'GET', headers = {}, **options):
        self.url = url
        self.method = method
        self.headers = {}
        self.creds = dict(api_user = options['api_user'], api_pass = options['api_pass'])
    
    def set_auth_header(self):
       self.headers['Authorization'] = "Basic %s" % (b64encode(self.creds['api_user'] + ':' + self.creds['api_pass']))
       return self
    
    @staticmethod
    def with_signature(url, method = 'GET', headers = {}, **options):
        request = DeliciousRequest(url, method, headers, **options)
        return request.set_auth_header()
    
class DeliciousResponse:
    def __init__(self, raw_response):
        self.raw_response = raw_response
    
    def json_parse(self):
       if not hasattr(self, 'json_response'):
           root = ET.fromstring(self.raw_response)
           post_list = [child.attrib for child in root]
           self.json_response = json.dumps({'posts': post_list})    
       return self.json_response
          
     
    def __str__(self):
        return self.raw_response           


class DeliciousApiClient:
    BASE_URL = DELICIOUS_API_BASE_URL
    
    def __init__(self, request):
        self.request = request
    
    def get_all_bookmarks(self):
        
        bookmark_request = DeliciousRequest.with_signature(url = '%s%s' % (self.BASE_URL, 'posts/all'), api_user = API_USER, api_pass = API_PASS)
        
        try:
            response = requests.get(bookmark_request.url, headers = bookmark_request.headers)
            response.encoding = 'utf-8'
            
            self.response = response
            status_code = self.response.status_code
            
            # Error
            if status_code >= 400:
                if status_code == 401:
                    raise Exception('Authorization Failed'); 
                
                raise Exception('Error receving bookmark data');
            
            # Success
            return DeliciousResponse(self.parse_response())

        except Exception as e:
            #return self.parse_response()
            self.set_error(e)
            raise e
    
    def parse_response(self):
        return self.response.text.encode('utf-8').strip()
        
    def set_error(self, error):
        self.api_error = {
            'message': str(error),
            'status':  self.response.status_code
        }
        #print dir(error)
        #print self.api_error
    
    def get_error(self):
        return self.api_error
    
