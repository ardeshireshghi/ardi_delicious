# Import flask and template operators
from flask import Flask

# Import SQLAlchemy
#from flask.ext.sqlalchemy import SQLAlchemy

# Define the WSGI application object
delicious_app = Flask(__name__)

# Configurations
delicious_app.config.from_object('config')

# Define the database object which is imported
# by modules and controllers
#db = SQLAlchemy(app)

# Sample HTTP error handling
@delicious_app.errorhandler(404)
def not_found(error):
    return 'Route not found'

# Import a module / component using its blueprint handler variable (mod_auth)
from app.controllers.bookmarks import bookmarks_bp as bookmarks


# Register blueprint(s)
delicious_app.register_blueprint(bookmarks)
# app.register_blueprint(xyz_module)
# ..

# Build the database:
# This will create the database file using SQLAlchemy
#db.create_all()
